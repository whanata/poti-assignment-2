var categories_xml = $.ajax({
    type: 'GET',
    url: 'xml/categories.xml',
    cache: false,
    dataType: 'xml'
});
var products_xml = $.ajax({
    type: 'GET',
    url: 'xml/products.xml',
    cache: false,
    dataType: 'xml'
});

$.when(categories_xml, products_xml).done(function(categories_xml, products_xml) {
    var first = true;
    var body = document.querySelector('#body');
    var ol = document.createElement('ol');
    ol.setAttribute('type', 'i');

    $(categories_xml).find('Categories').each(function(index) {
        var category_id = $(this).find('CategoryID').text();
        var li = document.createElement('li');
        li.setAttribute('id', category_id);
        li.setAttribute('onClick', 'category_description(this.id)');
        var anchor = document.createElement('a');
        anchor.setAttribute('href', '#');
        anchor.innerHTML = $(this).find('CategoryName').text();
        li.appendChild(anchor);
        ol.appendChild(li);

        var outside_li = document.createElement('li');
        outside_li.setAttribute('class', 'inside-li');
        var inside_ol = document.createElement('ol');
        inside_ol.setAttribute('type', 'a');

        $(products_xml).find('Products').each(function(index) {
            if (category_id == $(this).find('CategoryID').text()) {
                var product_id = $(this).find('ProductID').text();
                var inside_li = document.createElement('li');
                inside_li.setAttribute('id', product_id);
                inside_li.setAttribute('onClick', 'product_description(this.id)');
                var anchor = document.createElement('a');
                anchor.setAttribute('href', '#');
                anchor.innerHTML = $(this).find('ProductName').text();
                inside_li.appendChild(anchor);
                inside_ol.appendChild(inside_li);
            }
        });
        if ($(inside_ol).children().length > 0) {
            outside_li.appendChild(inside_ol);
            ol.appendChild(outside_li);
        }
    });
    body.appendChild(ol);
});

function open_modal() {
    $('body').attr('class', 'modal-open');
    var div = document.createElement('div');
    div.setAttribute('class', 'modal-backdrop fade in');
    $(div).appendTo($('body'));
    $('#modal').addClass('modal fade in');
    $('#modal').attr('style', 'display: block;');
}

function close_modal() {
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    $('#modal').removeClass('in');
    $('#modal').attr('style', 'style="display: none;"');
}

function category_description(category_id) {
    $.when(categories_xml, products_xml).done(function(categories_xml, products_xml) {
        $(categories_xml).find('Categories').each(function(index) {
            if (category_id == $(this).find('CategoryID').text()) {
                var description = $(this).find('Description').text();
                console.log(description);
                $('#modal').find('#heading')[0].innerHTML = $(this).find('CategoryName').text();
                $('#modal').find('#description')[0].innerHTML = description;
                open_modal();
            }
        });
    });
}

function product_description(product_id) {
    $.when(categories_xml, products_xml).done(function(categories_xml, products_xml) {
        $(products_xml).find('Products').each(function(index) {
            if (product_id == $(this).find('ProductID').text()) {
                var description = $(this).find('QuantityPerUnit').text() + ' for $' + $(this).find('UnitPrice').text();
                console.log(description);
                $('#modal').find('#heading')[0].innerHTML = $(this).find('ProductName').text();
                $('#modal').find('#description')[0].innerHTML = description;
                open_modal();
            }
        });
    });
}
